package study;

import com.github.javafaker.Faker;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import study.offer.Offer;
import study.offer.OfferRepository;
import study.product.Product;
import study.product.ProductRepository;
import study.seller.Seller;
import study.seller.SellerRepository;
import study.stock.Stock;
import study.stock.StockRepository;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class PopulateUtil {

    private ProductRepository productRepository;
    private SellerRepository sellerRepository;
    private StockRepository stockRepository;
    private OfferRepository offerRepository;

    private static final Faker faker = new Faker(new Random(System.currentTimeMillis()));

    @PostConstruct
    public void populate() {
        List<Product> products = create(getRandomNumber(200, 300), () -> Product.builder()
                .description(faker.lorem().paragraph())
                .name(faker.commerce().productName())
                .build());

        List<Seller> sellers = create(getRandomNumber(200, 300), () -> Seller.builder()
                .name(faker.company().name())
                .build());

        productRepository.save(products);
        sellerRepository.save(sellers);

        List<Offer> offers = new ArrayList<>();
        products.forEach(product -> offers.addAll(create(getRandomNumber(1, 12), () -> {
            Double basePrice = faker.number().randomDouble(2, 10, 5000);
            return Offer.builder()
                    .product(product.getId())
                    .seller(getRandom(sellers).getId())
                    .price(randomPrice(basePrice))
                    .build();
        })));

        offers.forEach(offer -> {
            Stock stock = Stock.builder()
                    .quantity(faker.number().numberBetween(0, 100))
                    .build();
            stockRepository.save(stock);
            offer.setStock(stock.getId());
            offerRepository.save(offer);
        });
        System.out.println("ok");
    }

    private BigDecimal randomPrice(Double basePrice) {
        return new BigDecimal(basePrice * faker.number().randomDouble(2, 0, 10));
    }

    private <T> T getRandom(List<T> list) {
        return list.get(getRandomNumber(0, list.size()));
    }

    private Integer getRandomNumber(int min, int max) {
        return faker.number().numberBetween(min, max);
    }

    private <T> List<T> create(Integer quantity, Supplier<T> supplier) {
        return range(0, quantity - 1).mapToObj(i -> supplier.get()).collect(toList());
    }


}
