package study.seller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static study.controller.ControllerUtil.resolve;

@RestController
@RequestMapping("seller")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SellerController {

    private SellerService sellerService;

    @RequestMapping("{seller}")
    public ResponseEntity get(@PathVariable String seller){
        return resolve(sellerService.findById(seller));
    }

    @RequestMapping()
    public Page<Seller> get(Pageable page){
        return sellerService.list(page);
    }

}
