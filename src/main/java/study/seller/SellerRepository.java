package study.seller;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface SellerRepository extends PagingAndSortingRepository<Seller, String>{
    List<Seller> findByIdIn(List<String> ids);
}
