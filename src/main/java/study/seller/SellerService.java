package study.seller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class SellerService {

    private SellerRepository sellerRepository;

    public Seller findById(String seller) {
        return sellerRepository.findOne(seller);
    }

    public Page<Seller> list(Pageable page) {
        return sellerRepository.findAll(page);
    }

    public List<Seller> findByIds(List<String> ids){
        return sellerRepository.findByIdIn(ids);
    }
}
