package study.product;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ProductService {

    private ProductRepository productRepository;

    public Product findById(String id) {
        return productRepository.findOne(id);
    }

    public Page<Product> list(Pageable page) {
        return productRepository.findAll(page);
    }
}
