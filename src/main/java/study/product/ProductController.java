package study.product;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static study.controller.ControllerUtil.resolve;

@RestController
@RequestMapping("product")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ProductController {

    private ProductService productService;

    @RequestMapping("{id}")
    public ResponseEntity get(@PathVariable String id) {
        return resolve(productService.findById(id));
    }

    @RequestMapping("")
    public Page<Product> list(Pageable page) {
        return productService.list(page);
    }

}
