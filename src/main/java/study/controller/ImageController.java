package study.controller;

import org.imgscalr.Scalr;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;

@Controller
@RequestMapping("image")
public class ImageController {

    @RequestMapping("{id}/{width}/{height}")
    public void image(@PathVariable String id, @PathVariable Integer width, @PathVariable Integer height, HttpServletResponse response){
        BufferedImage img = new BufferedImage(width/100, height/100, BufferedImage.TYPE_INT_ARGB);
        for(int y = 0; y < height/100; y++){
            for(int x = 0; x < width/100; x++){
                int a = (int)(Math.random()*256);
                int r = (int)(Math.random()*256);
                int g = (int)(Math.random()*256);
                int b = (int)(Math.random()*256);
                int p = (a<<24) | (r<<16) | (g<<8) | b;
                img.setRGB(x, y, p);
            }
        }
        try{
            ImageIO.write(Scalr.resize(img, Scalr.Method.SPEED, Scalr.Mode.AUTOMATIC, width, height, null), "png", response.getOutputStream());
        }catch(IOException e){
            System.out.println("Error: " + e);
        }
    }

}
