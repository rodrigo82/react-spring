package study.controller;

import org.springframework.http.ResponseEntity;

import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

public class ControllerUtil {

    public static ResponseEntity resolve(Object o){
        return o != null ? ok(o) : notFound().build();
    }

}
