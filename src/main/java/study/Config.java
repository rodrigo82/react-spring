package study;

import com.github.fakemongo.Fongo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;

@SpringBootApplication
public class Config extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Config.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(Config.class, args);
    }

    @Bean
    public MongoTemplate mongoTemplate() {
        return new MongoTemplate(new Fongo("Memory").getMongo(), "mem");
    }

}
