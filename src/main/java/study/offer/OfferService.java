package study.offer;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class OfferService {

    private OfferRepository offerRepository;

    public Offer findById(String id) {
        return offerRepository.findOne(id);
    }

    public List<Offer> findByProduct(String product) {
        return offerRepository.findByProduct(product);
    }

}
