package study.offer;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static study.controller.ControllerUtil.resolve;

@RestController
@RequestMapping("offer")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class OfferController {

    private OfferService offerService;

    @RequestMapping("{id}")
    public ResponseEntity get(@PathVariable String id){
        return resolve(offerService.findById(id));
    }

    @RequestMapping(params = {"product"})
    public List byProduct(String product){
        return offerService.findByProduct(product);
    }

}
