package study.offer;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface OfferRepository extends PagingAndSortingRepository<Offer, String>{
    List<Offer> findByProduct(String id);
}
