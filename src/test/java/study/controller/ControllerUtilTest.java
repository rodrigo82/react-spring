package study.controller;

import org.junit.Test;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static study.controller.ControllerUtil.resolve;

public class ControllerUtilTest {

    @Test
    public void resolveOk() throws Exception {
        ResponseEntity responseEntity = resolve(0);
        assertThat(responseEntity.getStatusCode().value()).isEqualTo(200);
    }

    @Test
    public void resolveNotFound() throws Exception {
        ResponseEntity responseEntity = resolve(null);
        assertThat(responseEntity.getStatusCode().value()).isEqualTo(404);
    }

}