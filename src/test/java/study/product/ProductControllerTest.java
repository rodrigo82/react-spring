package study.product;

import org.junit.Test;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ProductControllerTest {

    private ProductService productService = spy(new ProductService(null));
    private MockMvc mockMvc = MockMvcBuilders
            .standaloneSetup(new ProductController(productService))
            .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
            .build();

    @Test
    public void findById() throws Exception {
        doReturn(Product.builder().id("10").name("teste").description("teste").build()).when(productService).findById("10");
        mockMvc.perform(get("/product/10"))
                .andExpect(jsonPath("$.name", is("teste")))
                .andExpect(jsonPath("$.description", is("teste")));
    }

    @Test
    public void findByIdNotFound() throws Exception {
        doReturn(null).when(productService).findById("1");
        mockMvc.perform(get("/product/1")).andExpect(status().isNotFound());
    }

    @Test
    public void list() throws Exception {
        doReturn(new PageImpl<>(asList(Product.builder().id("1").build()), new PageRequest(0, 1), 1)).when(productService).list(any());
        mockMvc.perform((get("/product")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content[0].id", is("1")))
                .andExpect(jsonPath("$.totalElements", is(1)));
    }

}