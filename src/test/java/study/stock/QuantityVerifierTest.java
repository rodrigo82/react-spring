package study.stock;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class QuantityVerifierTest {

    private QuantityVerifier quantityVerifier;
    private static final Integer ULTIMAS_UNIDADES = 5;

    @Before
    public void init(){
        quantityVerifier = new QuantityVerifier(ULTIMAS_UNIDADES);
    }

    @Test
    public void notUltimasUnidades() throws Exception {
        Stock result = quantityVerifier.verify(Stock.builder().quantity(ULTIMAS_UNIDADES + 1).build());
        assertThat(result.isUltimasUnidades()).isFalse();
    }

    @Test
    public void ultimasUnidades() throws Exception {
        Stock result = quantityVerifier.verify(Stock.builder().quantity(ULTIMAS_UNIDADES - 1).build());
        assertThat(result.isUltimasUnidades()).isTrue();
    }

    @Test
    public void ultimasUnidadesIgual() throws Exception {
        Stock result = quantityVerifier.verify(Stock.builder().quantity(ULTIMAS_UNIDADES).build());
        assertThat(result.isUltimasUnidades()).isTrue();
    }

}