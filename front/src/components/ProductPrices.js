import React from 'react'
import Store from './Store'
import {Grid, Cell} from 'react-mdl'
import Label from './Label'
import ProductPrice from './ProductPrice'

export default React.createClass({
	componentWillMount() {
		this.setState({offers: []})
		Store.offer.findByProduct(this.props.product).then(result => this.setState({offers: result.data}));
	},
	render() {
		return (			
			<div>
				{this.state.offers.map(function (offer) {
				    return <ProductPrice key={offer.id} offer={offer} />
				})}				    		
			</div>
		);
	}
});