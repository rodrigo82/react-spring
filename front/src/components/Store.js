import ProductStore from '../store/ProductStore'
import OfferStore from '../store/OfferStore'
import SellerStore from '../store/SellerStore'
import StockStore from '../store/StockStore'

export default {
	product: ProductStore,
	offer: OfferStore,
	seller: SellerStore,
	stock: StockStore
}