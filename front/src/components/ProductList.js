import React from 'react';
import {Grid, Cell} from 'react-mdl'
import ProductCard from './ProductCard'
import Store from './Store'

export default React.createClass({
	componentWillMount() {
        Store.product.getAll().then(result => this.setState({products: result.data.content}));            
    },
    render(){
        return (this.state || {}).products ? 
        			(<Grid>{this.state.products.map(product => <Cell col={3} phone={12} key={product.id}><ProductCard product={product} /></Cell>)}</Grid>)
        				: (<div></div>)
    }
});