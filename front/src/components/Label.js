import React from 'react';
import {Grid, Cell} from 'react-mdl'
import Store from './Store'

export default React.createClass({
    componentWillMount: function() {
        this.setState({object: {}});
        Store[this.props.type].findById(this.props.entityId).then(result => this.setState({object: result.data}));
    },
    render() {
        return (<span>{this.state.object[this.props.field]}</span>);
    }
});