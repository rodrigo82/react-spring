import React from 'react';
import axios from 'axios';
import Label from './Label';
import Store from './Store';

export default React.createClass({
    componentWillMount: function() {
        this.state = {stock: {}};
        Store.stock.findById(this.props.offer.stock).then(result => this.setState({stock: result.data}));
    },
    render() {
        return (
            <div>
                <div>
                    <div>
                        <h2><Label type='seller' field='name' entityId={this.props.offer.seller} /></h2>
                    </div>
                    <div>R$ {this.props.offer.price.toFixed(2)}</div>
                    <div>
                        <button disabled={!this.state.stock.hasStock}>Comprar</button>
                        {!this.state.stock.hasStock ? <span>Item sem estoque</span> : null}
                        {this.state.stock.ultimasUnidades}
                    </div>
                </div>
            </div>
            );
    }
});