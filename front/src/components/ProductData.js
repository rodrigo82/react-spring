import React from 'react';
import Store from './Store'

export default React.createClass({
	componentWillMount() {
		this.setState({product: {}})
		Store.product.findById(this.props.product).then(result => this.setState({product: result.data}))
	},
    render() {
        return (
            <div>
			   <div>
			       <h2>{this.state.product.name}</h2>
			   </div>
			   <div>{this.state.product.description}</div>
			</div>
        );
    }
});