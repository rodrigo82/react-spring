import React from 'react'
import { Link } from 'react-router'
import {Card, CardTitle, CardText, CardActions, Button} from 'react-mdl'

export default (props) =>
<Card style={{height: '300px', background: 'url(/image/' + props.product.id + '/400/400)'}}>
	<CardTitle expand>{props.product.name}</CardTitle>
	<CardText>{props.product.description}</CardText>
	<CardActions><Link to={'product/'+props.product.id}><Button>Detalhes</Button></Link></CardActions>	
</Card>
