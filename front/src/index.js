import React from 'react'
import { render } from 'react-dom'
import { Router, Route, hashHistory } from 'react-router'
import Home from './Home'
import Products from './Products'
import Product from './Product'

import './styles/index.scss'
require('../node_modules/react-mdl/extra/material.css')
require('../node_modules/react-mdl/extra/material.js')
require('../node_modules/react-mdl/extra/css/material.grey-indigo.min.css')

render((
  <Router history={hashHistory}>
    <Route path='/' component={Home}>
        <Route path='product' component={Products}/>
        <Route path='product/:id' component={Product}/>
    </Route>
  </Router>
), document.getElementById('app'))