import React from 'react';
import {Layout, Content, Header, Navigation, Drawer} from 'react-mdl';

export default (props) =>
<div>
	<Layout>
    	<Header title="Poc" className="top-bar">
        	<Navigation>
                <a href="">Link</a>
                <a href="">Link</a>
                <a href="">Link</a>
                <a href="">Link</a>
            </Navigation>
    	</Header>
    	<Drawer title="Title">
            <Navigation>
                <a href="">Link</a>
                <a href="">Link</a>
                <a href="">Link</a>
                <a href="">Link</a>
            </Navigation>
        </Drawer>
    	<Content>{props.children}</Content>
    </Layout>
</div>