import Stomp from 'stompjs'
import SockJS from 'sockjs-client'

export default function (endpoint, callback) {
    var over = Stomp.over(new SockJS("/sock"));
    over.connect(function () {
        over.subscribe(endpoint, function (event) {
            callback(JSON.parse(event.body));
        });
    });
}