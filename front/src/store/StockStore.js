import axios from 'axios';

const resource = 'stock';

export default {
	findById: function(id){
		return axios.get(`${resource}/${id}`);
	}
};