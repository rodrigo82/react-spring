import axios from 'axios';

const resource = 'product';

export default {
	getAll: function(){
		return axios.get(resource); 
	},
	findById: function(id){
		return axios.get(`${resource}/${id}`);
	}
};