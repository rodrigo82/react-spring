import axios from 'axios';

const resource = 'offer';

export default {
	findByProduct: function(product){
		return axios.get(`${resource}?product=${product}`);
	}
};