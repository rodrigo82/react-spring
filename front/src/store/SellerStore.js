import axios from 'axios';

const resource = 'seller';

export default {
	findById: function(id){
		return axios.get(`${resource}/${id}`);
	}
};