import React from 'react';
import ProductData from './components/ProductData'
import ProductPrices from './components/ProductPrices'
import {Grid, Cell} from 'react-mdl'

export default (props) =>
<Grid style={{width: '70%'}}>
    <Cell col={6}>
        <ProductData product={this.props.params.id} />
    </Cell>
    <Cell col={6}>
        <ProductPrices product={this.props.params.id} />
    </Cell>
</Grid>